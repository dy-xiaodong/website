module.exports = {
    root: true,
    rules: {
        "camelcase": 0,
        "vue/multi-word-component-names": "off",
        "vue/no-multiple-template-root": "off",
        "vue/no-reserved-keys": "off"
    },
    'extends': [
        'plugin:vue/essential',
    ]
}